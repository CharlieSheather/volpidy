import sys
import time
import signal
import spidev
import minidsp
import requests
import RPi.GPIO as GPIO

spi_ch = 0
mute_led_pin = 4
mute_pin = 23
prev_pin = 24
playpause_pin = 17
next_pin = 27

min_db = -42
max_db = -1
mute_at_db = min_db + 1

led_pwm_freq = 10
max_led_pwm = 90

def read_adc(adc_ch, vref = 3.3):

    # Make sure ADC channel is 0 or 1
    if adc_ch != 0:
        adc_ch = 1

    # Construct SPI message
    #  First bit (Start): Logic high (1)
    #  Second bit (SGL/DIFF): 1 to select single mode
    #  Third bit (ODD/SIGN): Select channel (0 or 1)
    #  Fourth bit (MSFB): 0 for LSB first
    #  Next 12 bits: 0 (don't care)
    msg = 0b11
    msg = ((msg << 1) + adc_ch) << 5
    msg = [msg, 0b00000000]
    reply = spi.xfer2(msg)

    # Construct single integer out of the reply (2 bytes)
    adc = 0
    for n in reply:
        adc = (adc << 8) + n

    # Last bit (0) is not part of ADC value, shift to remove it
    adc = adc >> 1

    # Calculate voltage form ADC value
    voltage = (vref * adc) / 1024

    return voltage

def translate_value(value, leftMin, leftMax, rightMin, rightMax):
    # Figure out how 'wide' each range is
    leftSpan = leftMax - leftMin
    rightSpan = rightMax - rightMin

    # Convert the left range into a 0-1 range (float)
    valueScaled = float(value - leftMin) / float(leftSpan)

    # Convert the 0-1 range into a value in the right range.
    return rightMin + (valueScaled * rightSpan)

def mopidy_call(rpc_method):
    return requests.request(
        method = "post",
        url = "http://192.168.2.205:6680/mopidy/rpc",
        json = {
            "jsonrpc": "2.0",
            "id": 1,
            "method": rpc_method
        }
    )

def mopidy_running():
    try:
        r = mopidy_call("core.playback.get_state")
        running = r.status_code == 200
    except:
        running = False

    return running

def mopidy_playpause(channel):
    json = mopidy_call("core.playback.get_state").json()
    if json["result"] != "playing":
        mopidy_call("core.playback.play")
        print("Playing")
    else:
        mopidy_call("core.playback.pause")
        print("Paused")

def mopidy_prev(channel):
    print("Previous track")
    mopidy_call("core.playback.previous")

def mopidy_next(channel):
    print("Next track")
    mopidy_call("core.playback.next")

def minidsp_mute_callback(channel, current_vol):
    if current_vol > mute_at_db:
        minidsp_set_mute(not board.getMute())

def minidsp_set_mute(new_mute):
    tic = time.perf_counter()
    board.setMute(new_mute)
    if new_mute:
        GPIO.output(mute_led_pin, GPIO.HIGH)
        print("Muting")

    else:
        GPIO.output(mute_led_pin, GPIO.LOW)
        print("Unmuting")

    toc = time.perf_counter()
    print(f"Set mute in {toc - tic:0.5f}s")

def minidsp_set_vol(new_vol):
    current_mute = board.getMute()
    if (new_vol <= mute_at_db) != current_mute:
        minidsp_set_mute(new_vol <= mute_at_db)

    register_mute_event(new_vol)
    tic = time.perf_counter()
    board.setVolume(new_vol)
    toc = time.perf_counter()
    print(f"Set volume to: {new_vol}dB in {toc - tic:0.5f}s")

def signal_handler(sig, frame):
    print("Cleaning up, exiting...")
    board.close()
    # pwm.stop()
    GPIO.cleanup()
    sys.exit(0)

def as_db(value):
    return round(float(value))

def register_mute_event(current_vol):
    GPIO.remove_event_detect(mute_pin)
    mute_callback = lambda channel, current_vol=current_vol: minidsp_mute_callback(channel, current_vol)
    GPIO.add_event_detect(mute_pin, GPIO.FALLING, callback=mute_callback, bouncetime=300)

if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal_handler)

    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(mute_led_pin, GPIO.OUT)
    # pwm = GPIO.PWM(mute_led_pin, led_pwm_freq)

    GPIO.setup(mute_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(prev_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(playpause_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(next_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    running = mopidy_running()
    while not running:
        print("Waiting for mopidy to start...")
        time.sleep(1)

    print("Mopidy started")
    
    print("Connecting to minidsp...")
    board = minidsp.board_2x4hd.Board2x4HD()
    current_vol = as_db(board.getVolume())
    current_mute = board.getMute()

    # if current_mute:
    #     pwm.start(max_led_pwm)
    # else:
    #     pwm.start(0)

    print("Current volume: " + str(current_vol) + "dB")
    print("Muted" if current_mute else "Unmuted")

    spi = spidev.SpiDev(0, spi_ch)
    spi.max_speed_hz = 1200000

    register_mute_event(current_vol)

    GPIO.add_event_detect(playpause_pin, GPIO.FALLING, callback=mopidy_playpause, bouncetime=300)
    GPIO.add_event_detect(prev_pin, GPIO.FALLING, callback=mopidy_prev, bouncetime=300)
    GPIO.add_event_detect(next_pin, GPIO.FALLING, callback=mopidy_next, bouncetime=300)

    while True:
        pot = read_adc(0, 100)
        new_vol = as_db(translate_value(pot, 0, 100, min_db, max_db))

        if abs(new_vol - current_vol) > 1:
            current_vol = new_vol
            minidsp_set_vol(new_vol)
            time.sleep(0.5)
        else:
            time.sleep(0.1)